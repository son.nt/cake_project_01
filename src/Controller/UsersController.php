<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['register']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $key = $this->request->getQuery('key');
        if($key){
            $query = $this->Users->find('all')->where(['username like'=>'%'.$key.'%']);
        }else{
            $query = $this->Users;
        }
        $users = $this->paginate($query);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            if (!empty($this->request->data['image']['name'])) {
                $filename = $this->request->data['image']['name'];
                $uploadPath = WWW_ROOT.'img/';
                $uploadFile = $uploadPath . $filename;

                if (move_uploaded_file($this->request->data['image']['tmp_name'], $uploadFile)) {
                    $this->request->data['image'] = $filename;
                }
            }
            $user = $this->Users->patchEntity($user, $this->request->getData());

//            if(!$user->getErrors)
//            {
//            $image = $this->request->getData('image_file');
//            $name = $image->getClientFilename();
//            $targetPath = WWW_ROOT.'img'.DS.$name;
//
//            if($name)
//                $image->
//            exit;

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }
    public function search()
    {

        $this->request->allowMethod('ajax');

        $keyword = $this->request->query('keyword');

        $query = $this->Tags->find('all',[
            'conditions' => ['name LIKE'=>'%'.$keyword.'%'],
            'order' => ['Tags.id'=>'DESC'],
            'limit' => 10
        ]);

        $this->set('tags', $this->paginate($query));
        $this->set('_serialize', ['tags']);

    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!empty($this->request->data['image']['name'])) {
                $filename = $this->request->data['image']['name'];
                $uploadPath = WWW_ROOT.'img/';
                $uploadFile = $uploadPath . $filename;

                if (move_uploaded_file($this->request->data['image']['tmp_name'], $uploadFile)) {
                    $this->request->data['image'] = $filename;
                }
            }
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['action'=>'index']);
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }
    public function register()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Sign Up Success.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Registration failed. Please, try again.'));
        }
        $this->set(compact('user'));
    }


    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }
}
